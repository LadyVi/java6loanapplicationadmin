package java6.credit.loan.application.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public final class LoanApplication {
    @Id
    @GeneratedValue
    @Column
    private long id;

    @Column
    private LoanApplicationStatus loanApplicationStatus;

    @Column
    private String userId;

    @Column
    private BigDecimal amount;

    @Column
    private Date dateFrom;

    @Column
    private Date dateTo;

    @Column
    private String rejectionReason;

    @Column
    private String type;

    @ManyToOne
        private User user;

    public LoanApplicationStatus getLoanApplicationStatus() {
        return loanApplicationStatus;
    }

    public void setLoanApplicationStatus(LoanApplicationStatus loanApplicationStatus) {
        this.loanApplicationStatus = loanApplicationStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "LoanApplication{" +
                "id=" + id +
                ", loanApplicationStatus=" + loanApplicationStatus +
                ", userId='" + userId + '\'' +
                ", amount=" + amount +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                ", rejectionReason='" + rejectionReason + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
