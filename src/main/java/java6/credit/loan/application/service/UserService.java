package java6.credit.loan.application.service;

import java6.credit.loan.application.exception.LoanAppllicationNotFoundException;
import java6.credit.loan.application.model.User;
import java6.credit.loan.application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserService {

    @Autowired

    private UserRepository userRepository;

    public User getUserById(Long id) {
        return userRepository.findById(id).orElseThrow(
                () -> new LoanAppllicationNotFoundException(
                        "User with id " + id + "not found!"));
    }

    public List<User> getAllUsers(Long id) {
        return userRepository.findAll();
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public User findUserByPhoneNumber(String phoneNumber) {
        return userRepository.findByPhoneNumber(phoneNumber);
    }

}
