package java6.credit.loan.application.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class User {

    @Id
    @GeneratedValue
    @Column

    private long userPk;

    @Column
    private String Name;

    @Column
    private String lastName;

    @Column
    private String personalCode;

    @Column
    private String phoneNumber;

    @Column
    private String email;

    @Column
    @OneToMany(fetch = FetchType.EAGER)
    private Set<LoanApplication> loanApplications;


    public long getUserPk() {
        return userPk;
    }

    public void setUserPk(long userPk) {
        this.userPk = userPk;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(String personalCode) {
        this.personalCode = personalCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<LoanApplication> getLoanApplicationSet() {
        return loanApplications;
    }

    public void setLoanApplicationSet(Set<LoanApplication> loanApplicationSet) {
        this.loanApplications = loanApplications   ;
    }

    @Override
    public String toString() {
        return "User{" +
                "userPk=" + userPk +
                ", Name='" + Name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", personalCode='" + personalCode + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", loanApplications=" + loanApplications +
                '}';
    }
}
