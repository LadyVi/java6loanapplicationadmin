package java6.credit.loan.application.model;

public enum loanApplicationStatus {
    PENDING,
    REJECTED,
    APPROVED,
    DELETED
}
