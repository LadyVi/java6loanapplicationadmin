package java6.credit.loan.application.repository;

import java6.credit.loan.application.model.LoanApplication;
import org.springframework.data.jpa.repository.JpaRepository;

@org.springframework.stereotype.Repository
public interface LoanApplicationRepository
        extends JpaRepository<LoanApplication, Long> {

    //updateApplicationStatus (reject vai approve)

}
