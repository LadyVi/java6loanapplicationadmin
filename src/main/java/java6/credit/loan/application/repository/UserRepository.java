package java6.credit.loan.application.repository;

import java6.credit.loan.application.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

@org.springframework.stereotype.Repository
public interface UserRepository extends JpaRepository<User, Long> {

    //search user
    //findUserByPhone

    User findByPhoneNumber(String phoneNumber);


}
