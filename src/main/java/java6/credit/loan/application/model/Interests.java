package java6.credit.loan.application.model;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class Interests {
    @Id
    @GeneratedValue

    @Column
    private String type;

    @Column
    private Long rate;

    @Column
    private Long id;

    @OneToMany(mappedBy = "interest",
            fetch = FetchType.EAGER)
    private Set<LoanApplication> loanApplicationSet;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getRate() {
        return rate;
    }

    public void setRate(Long rate) {
        this.rate = rate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<LoanApplication> getLoanApplicationSet() {
        return loanApplicationSet;
    }

    public void setLoanApplicationSet(Set<LoanApplication> loanApplicationSet) {
        this.loanApplicationSet = loanApplicationSet;
    }

    @Override
    public String toString() {
        return "Interests{" +
                "type='" + type + '\'' +
                ", rate=" + rate +
                ", id=" + id +
                ", loanApplicationSet=" + loanApplicationSet +
                '}';
    }
}
