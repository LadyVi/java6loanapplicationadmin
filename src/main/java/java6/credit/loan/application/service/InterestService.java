package java6.credit.loan.application.service;

import java6.credit.loan.application.model.Interests;
import java6.credit.loan.application.repository.InterestsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InterestService {

    @Autowired
    private InterestsRepository interestsRepository;

    public List<Interests> getAllInterests() {
        return interestsRepository.findAll();
    }

    public Interests getInterestsById(Long id) {
        return interestsRepository.getOne(id);
    }

    public List<Interests> getAllByType() {
        return interestsRepository.findAllByType();
    }

}
