package java6.credit.loan.application.service;

import java6.credit.loan.application.exception.LoanAppllicationNotFoundException;
import java6.credit.loan.application.model.LoanApplication;
import java6.credit.loan.application.repository.LoanApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java6.credit.loan.application.model.loanApplicationStatus;

import java.util.List;
import java.util.Optional;

@Component
public class LoanApplicationService {

    @Autowired

    private LoanApplicationRepository loanApplicationRepository;

    private LoanApplication getLoanApplicationById(Long id) {
        return loanApplicationRepository.
                findById(id).orElseThrow(
                () -> new LoanAppllicationNotFoundException(
                        "loan application with id " + id + "not found!"));
    }

    public List<LoanApplication> getAllLoanApplications() {
        return loanApplicationRepository.findAll();
    }

    public void changeLoanApplicationStatus(Long id,
                                            loanApplicationStatus status) {
        loanApplicationRepository.findById(id).ifPresent(
                loanApplication -> {
                    loanApplication.setLoanApplicationStatus(status);
                    loanApplicationRepository.save(loanApplication);
                }
        );


    }

    public void saveLoanApplication(LoanApplication loanApplication) {
        loanApplicationRepository.save(loanApplication);
    }
}
