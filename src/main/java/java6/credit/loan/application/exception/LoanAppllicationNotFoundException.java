package java6.credit.loan.application.exception;

public class
LoanAppllicationNotFoundException
        extends RuntimeException{
    public LoanAppllicationNotFoundException(String message) {
        super(message);
    }
}
